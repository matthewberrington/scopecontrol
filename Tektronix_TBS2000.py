import numpy as np
from visa import *
import time
from glob import glob
import re
import scipy.io as sio
import os
import datetime
from purephotonicscontrol.purephotonicscontrol import logger

def Initialise(rm,logger_):
    global Tektronix_TBS2000
    logger = logger_
    Tektronix_TBS2000 = rm.open_resource("USB0::0x0699::0x03C6::C020083::INSTR")
    Tektronix_TBS2000.write("*IDN?")
    logger.info("You are using the:" + Tektronix_TBS2000.read().strip('\n'))
    print("Connected to Tektronix_TBS2000")
    
    Tektronix_TBS2000.values_format.is_binary = True
    #the following sets the binary format of the data from the scope. 
    #see python struct module: 'H' represents an unsigned short (2 bytes)
    #also see the TDS540 manual, page 2-89 for binary encoding data strings
    Tektronix_TBS2000.values_format.datatype = 'H' 
    Tektronix_TBS2000.values_format.is_big_endian = True
    
    #store the data directly into a numpy array for easier manipulation
    Tektronix_TBS2000.values_format.container = np.array
    #set the data encoding to binary and then query
    Tektronix_TBS2000.write("DATA:ENCDG RPB")
    #Tektronix_TBS2000.write("AQUIRE:STATE OFF")
    
    return(Tektronix_TBS2000)

def trace_settings():
    
    #read the data source   
        
    Tektronix_TBS2000.write("HOR:RECO?")
    print('the data record length is: ' + Tektronix_TBS2000.read())    
    
    Tektronix_TBS2000.write("DATA:START 1")
    
    Tektronix_TBS2000.write("DATA:START?")
    print('start reading data at: ' + Tektronix_TBS2000.read())
    
    #Tektronix_TBS2000.write("DATA:STOP 15000")
    
    Tektronix_TBS2000.write("DATA:STOP?")
    print('stop reading data at: ' + Tektronix_TBS2000.read())
  
    
def collect_trace():

    #record initial state of scope
    initstate = int(Tektronix_TBS2000.query("ACQUIRE:STATE?"))
    Tektronix_TBS2000.write("ACQUIRE:STATE STOP")

    #make sure all channels are displayed
    Tektronix_TBS2000.write("SELECT:CH1 ON")
    Tektronix_TBS2000.write("SELECT:CH2 ON")
    Tektronix_TBS2000.write("SELECT:CH3 ON")
    Tektronix_TBS2000.write("SELECT:CH4 ON")
    
    [xtime,data_ch1] = get_channel_data('CH1')
    [xtime,data_ch2] = get_channel_data('CH2')
    [xtime,data_ch3] = get_channel_data('CH3')
    [xtime,data_ch4] = get_channel_data('CH4')    
    
    # If it was running, make it go again
    if initstate:
        Tektronix_TBS2000.write("ACQUIRE:STATE RUN")
    
    return(xtime,data_ch1, data_ch2, data_ch3,data_ch4)
    
def get_channel_data(chname):
    # Read a channel and get x and y in real units
    Tektronix_TBS2000.write("DATA:SOURCE "+chname)
    #NP = Tektronix_TBS2000.query('WFMOUTPRE:NR_PT?'); #gives wrong no. of points.
    NumPoints = Tektronix_TBS2000.query('HOR:RECO?');
    print(NumPoints)
    
    #Make sure you are recording full
    Tektronix_TBS2000.write('DATA:START 1')
    Tektronix_TBS2000.write('DATA:STOP '+NumPoints)
    Tektronix_TBS2000.write('WFMOUTPRE:ENCdg ASCii')
   
    
    raw = Tektronix_TBS2000.query_ascii_values('CURV?',container=np.array)
    yoffs = float(Tektronix_TBS2000.query('WFMPRE:YOFF?'))
    ymult = float(Tektronix_TBS2000.query('WFMPRE:YMULT?'))
    yzero = float(Tektronix_TBS2000.query('WFMPRE:YZERO?'))
    data_ch = ((raw - yoffs)*ymult) + yzero
    
    timestep = float(Tektronix_TBS2000.query('WFMPRE:XINCR?'));
    numpoints = float(Tektronix_TBS2000.query('WFMPRE:NR_PT?'));
    data_time = np.arange(numpoints)*timestep;
    
    return(data_time,data_ch)

def close_Tektronix_TBS2000():
    return rm.close()


def wait_until_ready():
    """ Waits until the Tektronix_TBS2000 is ready to be triggered
    """
    print('Waiting until Tektronix_TBS2000 is ready to be triggered')
    while True:
        Tektronix_TBS2000.write("TRIGGER:STATE?")
        status = Tektronix_TBS2000.read()
        time.sleep(0.1)
        if status == u'READY\n':
            break
    logger.info("Tektronix_TBS2000 ready to be triggered")
    
    
def wait_to_collect():
    """ Waits while the Tektronix_TBS2000 is collecting a trace
    """
    print('Waiting until Tektronix_TBS2000 finishes collecting a trace')
    while True:
        Tektronix_TBS2000.write("TRIGGER:STATE?")
        status = Tektronix_TBS2000.read()
        time.sleep(0.1)
        if status != u'TRIGGER\n':
            logger.info("Tektronix_TBS2000 trace collected")
            break
        

def set_scale_3v3(channel):
    """ Set the specific channel's scale to one optimised for signals in 0-3.3V range
    channel (string): Either "CH1", "CH2", "CH3" or "CH4"
    """
    Tektronix_TBS2000.write(channel + ":SCALE 0.350")        
        
        
def trigger_manually():
    print('Manually triggering Tektronix_TBS2000')
    Tektronix_TBS2000.write('TRIGGER FORCE')    

def horizontal_scale(scale):
    """ Set horizonal scale. Can only take certain value 
    """
    exponent = int(np.floor(np.log10(scale)))
    coefficient = int(np.round(scale/10**exponent))
    Tektronix_TBS2000.write('HORIZONTAL:SCALE '+str(coefficient) + 'E' + str(exponent))
    
def single_shot():
    """ Puts the Tektronix_TBS2000 in single shot mode, preparing it for a manual trigger
    """
    #Make sure Tektronix_TBS2000 is in normal trigger mode
    Tektronix_TBS2000.write('HORIZONTAL:DELAY:MODE OFF')
    Tektronix_TBS2000.write('TRIGGER:A:MODE NORMAL')
    #Set trigger to be 5% along the horizontal axis
    Tektronix_TBS2000.write("HORIZONTAL:MAIN:POSITION 5.00")
    #Enable singe sequence capture
    Tektronix_TBS2000.write("FPANEL:PRESS SINGLESEQ ")
    logger.info("Tektronix_TBS2000 placed in single shot mode")

def quick_capture(path=r""):

    [xtime, CH1, CH2, CH3, CH4] = collect_trace()

    filebase = path
    #Save the data as a matlab file, automatically incrementing with date
    filedate = time.strftime('%d%b%Y')+'_' #Todays date
    files = glob(filebase+filedate+'*.mat')
    
    #add number suffix that iterates as files are saved
    if len(files) == 0:
        filenum = str(1)
    else:
        filenum = str(max([int(re.search(filedate+'(\d+)',x).group(1)) for x in files])+1);
        
    sio.savemat(filebase+filedate+filenum, {'x':xtime,'y':[CH1,CH2,CH3,CH4]}, oned_as='row')
    print('file saved as '+filebase+filedate+filenum)
    logger.info("Scope traces saved as " + filebase + filedate + filenum)


def capture(base_path=r"C:\\Users\\lab\\Berrington\\DataLibrary\\"): 
    """ Grabs the data on the scope and stores in my main YY/MM/DD data library
    """    
    now = datetime.datetime.now()
    path = base_path + now.strftime("%Y\\%m\\%d\\")
    if not os.path.isdir(path):
        os.makedirs(path)
    
    [xtime, CH1, CH2, CH3, CH4] = collect_trace()
    
    #Save the data as a matlab file, automatically incrementing with date
    files = glob(path+'*.mat')
    if files == []: 
        file_num = 0
    else:
        file_num = max([int(file[56:-4]) for file in files])+1
        
    sio.savemat(path+'data'+str(file_num), {'x':xtime,'y':[CH1,CH2,CH3,CH4]}, oned_as='row')
    print('Trace saved to '+path+'data'+str(file_num)+'.mat')
    logger.info("Scope traces saved to " + path+'data'+str(file_num)+'.mat')


if __name__ == "__main__":
    general = logger.logger('general')
    rm = ResourceManager();
    Initialise(rm,general.log)
    capture()
    general.shutdown()
