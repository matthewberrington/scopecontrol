import usbtmc
import numpy as np
import matplotlib.pyplot as plt

def Initialise():
    Tektronix_TBS2000 = usbtmc.Instrument("USB::0x0699::0x03c6::INSTR")
    Tektronix_TBS2000.write("*IDN?")
    # logger.info("You are using the:" + Tektronix_TBS2000.read())
    print("Connected to Tektronix_TBS2000")
    
    # Tektronix_TBS2000.values_format.is_binary = True
    # #the following sets the binary format of the data from the scope. 
    # #see python struct module: 'H' represents an unsigned short (2 bytes)
    # #also see the TDS540 manual, page 2-89 for binary encoding data strings
    # Tektronix_TBS2000.values_format.datatype = 'H' 
    # Tektronix_TBS2000.values_format.is_big_endian = True
    
    # #store the data directly into a numpy array for easier manipulation
    # Tektronix_TBS2000.values_format.container = np.array
    
    # #set the data encoding to binary and then query
    # Tektronix_TBS2000.write("DATA:ENCDG RPB")
    #Tektronix_TBS2000.write("AQUIRE:STATE OFF")
    
    return(Tektronix_TBS2000)

def trace_settings(Tektronix_TBS2000):
    
    #read the data source
    
        
    Tektronix_TBS2000.write("HOR:RECO?")
    print('the data record length is: ' + Tektronix_TBS2000.read())
    
    
    Tektronix_TBS2000.write("DATA:START 1")
    
    Tektronix_TBS2000.write("DATA:START?")
    print('start reading data at: ' + Tektronix_TBS2000.read())
    

    
    #Tektronix_TBS2000.write("DATA:STOP 15000")
    
    Tektronix_TBS2000.write("DATA:STOP?")
    print('stop reading data at: ' + Tektronix_TBS2000.read())

def collect_scope_trace(Tektronix_TBS2000):

##     #set/query the width of each data point in bytes. should be 2
##     Tektronix_TBS2000.write("DATA:WID?")
##     print Tektronix_TBS2000.read()

    initstate = int(Tektronix_TBS2000.ask("ACQUIRE:STATE?"))
    Tektronix_TBS2000.write("ACQUIRE:STATE STOP")
    
    # Set horizontal record length to 200,000. Note, does not return it to original value
    Tektronix_TBS2000.write("HOR:MAIN:SCALE?")
    scale = Tektronix_TBS2000.read()
    Tektronix_TBS2000.write("HOR:RECORDLENGTH 2000000")
    Tektronix_TBS2000.write("HOR:FITTOSCREEN ON")
    Tektronix_TBS2000.write("HOR:MAIN:SCALE "+scale)
    
    [xtime,data_ch1] = get_channel_data(Tektronix_TBS2000,'CH1')
    [xtime,data_ch2] = get_channel_data(Tektronix_TBS2000,'CH2')
    [xtime,data_ch3] = get_channel_data(Tektronix_TBS2000,'CH3')
    [xtime,data_ch4] = get_channel_data(Tektronix_TBS2000,'CH4')    
    
    # If it was running, make it go again
    if initstate:
        Tektronix_TBS2000.write("ACQUIRE:STATE RUN")
    
    return(xtime,data_ch1, data_ch2, data_ch3,data_ch4)
def get_channel_data(Tektronix_TBS2000,chname):
        # Read a channel and get x and y in real units
    Tektronix_TBS2000.write("DATA:SOURCE "+chname)
    #NP = Tektronix_TBS2000.ask('WFMOUTPRE:NR_PT?'); #gives wrong no. of points.
    NumPoints = Tektronix_TBS2000.ask('HOR:RECO?');
    print(NumPoints)
    
    #Make sure you are recording full
    Tektronix_TBS2000.write('DATA:START 1')
    Tektronix_TBS2000.write('DATA:STOP '+NumPoints)
    Tektronix_TBS2000.write('WFMOUTPRE:ENCdg ASCii')
   
    
    raw = Tektronix_TBS2000.ask('CURV?')
    
    raw = np.array([float(i) for i in raw.split(',')])
    yoffs = float(Tektronix_TBS2000.ask('WFMPRE:YOFF?'))
    ymult = float(Tektronix_TBS2000.ask('WFMPRE:YMULT?'))
    yzero = float(Tektronix_TBS2000.ask('WFMPRE:YZERO?'))
    data_ch = ((raw - yoffs)*ymult) + yzero
    
    timestep = float(Tektronix_TBS2000.ask('WFMPRE:XINCR?'));
    numpoints = float(Tektronix_TBS2000.ask('WFMPRE:NR_PT?'));
    data_time = np.arange(numpoints)*timestep;
    
    return(data_time,data_ch)

def trigger_manually(Tektronix_TBS2000):
    Tektronix_TBS2000.write('TRIGGER FORCE')
    print('Manually triggering scope')

if __name__ == "__main__":
	Tektronix_TBS2000 = Initialise()
	(time, ch1, ch2, ch3, ch4) = collect_scope_trace(Tektronix_TBS2000)

	plt.style.use('dark_background')
	plt.plot(time,ch1, 'y', label='CH1')
	plt.plot(time,ch2, 'c', label='CH2')
	plt.plot(time,ch3, 'm', label='CH3')
	plt.plot(time,ch4, 'g', label='CH4')
	plt.legend()
	plt.show()
	Tektronix_TBS2000.close()