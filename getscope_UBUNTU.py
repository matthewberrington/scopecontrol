from Tektronix_TBS2000_UBUNTU import *
import numpy as np
import scipy.io as sio
import matplotlib.pyplot as plt
import time
from glob import glob
import os, re

def Capture():
    
    Scope = Initialise()
    num_runs = 1

    for ii in range(0,num_runs):
        trace_settings(Scope)
        [time_temp,CH1_temp, CH2_temp, CH3_temp,CH4_temp] = collect_scope_trace(Scope)

        if ii != 0:   
            CH1 = np.c_[CH1,CH1_temp]
            CH2 = np.c_[CH2,CH2_temp];
            CH3 = np.c_[CH3,CH3_temp]; 
            CH4 = np.c_[CH4,CH4_temp]; 
            xtime = np.c_[xtime,time_temp]; 
        else:   
            CH1=CH1_temp;
            CH2=CH2_temp;
            CH3=CH3_temp;
            CH4=CH4_temp;
            xtime=time_temp;

    filebase = r"/home/matt/Documents/scopecontrols/"
    #Save the data as a matlab file, automatically incrementing with date
    filedate = time.strftime('%d%b') #Todays date
    files = glob(filebase+filedate+'*.mat')
    if len(files)==0:
        filenum = str(1)
    else:
        filenum = str(max([int(re.search(filedate+'(\d+)',x).group(1)) for x in files])+1);
        
    sio.savemat(filebase+filedate+filenum, {'x':xtime,'y':[CH1,CH2,CH3,CH4]}, oned_as='row')
    print('file saved as '+filebase+filedate+filenum)
    #plt.figure(1)
    #plt.plot(CH1)
    #plt.plot(CH2)
    #plt.plot(CH3)
    #plt.plot(CH4)
    #plt.show()


    print("Measurement complete")

if __name__ == "__main__":
    Capture()